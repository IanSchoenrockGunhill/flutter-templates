import 'package:flutter/material.dart';
import 'package:flutter_templates/CreateAccount.dart';
import 'package:flutter_templates/ListViewDynamic.dart';
import 'package:flutter_templates/ListViewStatic.dart';
import 'package:flutter_templates/ListViewWebApi.dart';

import 'Login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Template',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ListViewWebApi(),
    );
  }
}

