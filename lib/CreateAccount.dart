import 'package:flutter/material.dart';

class CreateAccount extends StatefulWidget{
  CreateAccountState createState()=> CreateAccountState();
}

class CreateAccountState extends State<CreateAccount> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(40),
        child: AppBar(
          title: Text(
            "Create Account", ),
        ),),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(left: 16, top: 32, right: 16, bottom: 8),
              child: Container(
                  child: new TextField(
                    controller: firstNameController,
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "First Name",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16, vertical: 8),
              child: Container(
                  child: new TextField(
                    controller: lastNameController,
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "Last Name",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16, vertical: 8),
              child: Container(
                  child: new TextField(
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "Phone Number (Optional)",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16, vertical: 8),
              child: Container(
                  child: new TextField(
                    controller: emailController,
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "Email",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16, vertical: 8),
              child: Container(
                  child: new TextField(
                    controller: passwordController,
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "Password",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 16, vertical: 8),
              child: Container(
                  child: new TextField(
                    controller: confirmPasswordController,
                    style: TextStyle(color: Colors.blue),
                    decoration: InputDecoration(
                      hintText: "Confirm Password",
                      contentPadding: EdgeInsets.all(10),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey)
                      ),
                    ),
                  )
              ),
            ),
            new Container(
              width: double.infinity,
              child: new Padding(
                padding: EdgeInsets.only(left: 16, top: 10, right: 16),
                child: FlatButton(
                  color: Colors.blue,
                  onPressed: ()=>{},
                  child: Text("Create Account",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white
                    ),),

                ),),),

            new Container(
              width: double.infinity,
              child: new Padding(
                padding: EdgeInsets.only(left: 16, top: 10, right: 16),
                child: FlatButton(
                  color: Colors.transparent,
                  onPressed: () => {Navigator.pop(context)},
                  child: Text("Back",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.blue
                    ),),

                ),),)
          ],
        ),
      ),
    );
  }
}