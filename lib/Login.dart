import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget{
  LoginState createState()=> LoginState();
}

class LoginState extends State<Login>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login Template"),
      )
      ,
      body: Container(child: Column(children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 16, top: 40, right: 16, ),
          child: TextField(
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(8),
                hintText: "Username",
              border: OutlineInputBorder()
            ),
          ),
        ), Padding(
          padding: EdgeInsets.all(16),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(8),
                hintText: "Password",
                border: OutlineInputBorder(),
            ),
          ),
        ),
        new Container(
          width: double.infinity,
          child: new Padding(
            padding: EdgeInsets.only(left: 16, top: 10, right: 16),
            child: RaisedButton(
              color: Colors.blue,
              onPressed: ()=> {} ,
              child: Text("Login",
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),),

            ),),),

        new Container(
          width: double.infinity,
          child: new Padding(
            padding: EdgeInsets.only(left: 16, top: 10, right: 16),
            child: FlatButton(
              color: Colors.transparent,
              onPressed: ()=>{},
              child: Text("Forgot Password",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.blue,
                ),),

            ),),),

        new Container(
          width: double.infinity,
          child: new Padding(
            padding: EdgeInsets.only(left: 16, top: 10, right: 16),
            child: FlatButton(
              color: Colors.transparent,
              onPressed: ()=>{},
              child: Text("Back",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.blue,
                ),),

            ),),),

        new Expanded(child: Container(height: 20, width: 20,)),

        new Row(
          children: <Widget>[
            new Expanded(
              child: new Padding(
                  padding: EdgeInsets.only(left: 16, right: 8),
                  child: Divider(
                    color: Colors.black,
                  )),

            ),
            new Text(
              "OR",
              style: TextStyle(color: Colors.blue,),),
            new Expanded(
              child: new Padding(
                  padding: EdgeInsets.only(left: 8, right: 16),
                  child: Divider(
                    color: Colors.black,
                  )),

            )
          ],
        ),
        new Container(
          width: double.infinity,
          child: new Padding(
            padding: EdgeInsets.only(left: 16, top: 10, right: 16, bottom: 32),
            child: FlatButton(
              color: Colors.blue,
              onPressed: ()=>{},
              child: Text("Create Account",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white
                ),),

            ),),),
      ],
    )));
  }

}