import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListViewStatic extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Static List"),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Sun'),
          ),
          ListTile(
            title: Text('Moon'),
          ),
          ListTile(
            title: Text('Star'),
          ),
          ListTile(
            title: Text('Comet'),
          ),
          ListTile(
            title: Text('Asteroid'),
          ),
          ListTile(
            title: Text('Space'),
          ),
        ],
      ),
    );
  }

}