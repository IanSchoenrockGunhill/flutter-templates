import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListViewWebApi extends StatefulWidget {
  ListViewWebApiState createState() => ListViewWebApiState();
}

class ListViewWebApiState extends State<ListViewWebApi> {
  bool isLoading = true;
  var myTodos;

  @override
  void initState() {
    loadItems();
    super.initState();
  }

  loadItems() async {
    getItems().then((response) => {
          print(response),
          setState(() {
            isLoading = false;
            myTodos = response;
          })
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List From API"),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: myTodos != null ? myTodos.length : 0,
            itemBuilder: (context, index) {
              return isLoading
                  ? Container(
                      color: Colors.white,
                      child: Center(
                          child: CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.black),
                      )),
                    )
                  : InkWell(
                      onTap: () => {},
                      child: Padding(
                        padding: EdgeInsets.only(
                            right: 16, top: 8, left: 16, bottom: 24),
                        child: Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              myTodos[index].title != null
                                  ? myTodos[index].title
                                  : "",
                              style: TextStyle(
                                  fontSize: 24, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ));
            }),
      ),
    );
  }

  //This methods should be in a view model
  Future<List<dynamic>> getItems() async {
    var response = await http.get("https://jsonplaceholder.typicode.com/todos");
    var temp = response.toString();
    print(temp);

    var responseObject = json.decode(response.body);
    var items = responseObject.map((model) => Item.fromJson(model)).toList();

    print(items);
    return items;
  }
}

//This class should be in its own file in a model folder
class Todo {
  List<Item> items;

  Todo({this.items});

  factory Todo.fromJson(Map<String, dynamic> json) => Todo(
        items: List<Item>.from(json["articles"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "articles": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  int userId;
  int id;
  String title;
  bool completed;

  Item({
    this.userId,
    this.id,
    this.title,
    this.completed,
  });

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        completed: json["completed"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "id": id,
        "title": title,
        "completed": completed,
      };
}
